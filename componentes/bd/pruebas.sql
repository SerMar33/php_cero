/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pruebas

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-11-15 21:31:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `estudiante`
-- ----------------------------
DROP TABLE IF EXISTS `estudiante`;
CREATE TABLE `estudiante` (
  `estudiante_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `estudiante_ced` char(10) DEFAULT NULL,
  `estudiante_nombre` char(70) DEFAULT NULL,
  `estudiante_sexo` enum('M','F') DEFAULT NULL,
  `estudiante_nacimiento` date DEFAULT NULL,
  `estudiante_direcc` varchar(100) DEFAULT NULL,
  `estudiante_telf` char(11) DEFAULT NULL,
  PRIMARY KEY (`estudiante_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estudiante
-- ----------------------------
INSERT INTO `estudiante` VALUES ('5', '11340466', 'Maria Carolina Torres', 'F', null, 'Valera, sector la Beatriz, apto 2-6', '04162832738');
INSERT INTO `estudiante` VALUES ('12', '56780890', 'Jose Ocanto', 'M', null, 'Trujillo, av Principal, av 4', '04162832738');
INSERT INTO `estudiante` VALUES ('13', '9701237', 'Amanble Hernandez', 'F', null, 'Bocono, sector la Vega, casa nro 407', '04163872837');
INSERT INTO `estudiante` VALUES ('16', '18378237', 'Susana Bracamontes', 'F', null, 'Caracas, sector 3 de Enero, apto 2-4', '04163878378');
INSERT INTO `estudiante` VALUES ('17', '10893230', 'Luis Medez', 'F', null, 'Trujillo', '04143039309');
INSERT INTO `estudiante` VALUES ('18', '5633679', 'Maria Damalis Rojo', 'F', null, 'Bocono, secto el Saman', '04163827282');
INSERT INTO `estudiante` VALUES ('20', '90487322', 'Rafael Hernandez', 'F', null, 'Trujillo', '04163383229');
INSERT INTO `estudiante` VALUES ('26', '11340403', 'Jesus Jerez', 'F', null, 'Trujillo, casa 303', '04143029209');
