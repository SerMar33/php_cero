<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8"/>
    <title>SENTENCIAS</title>
    <link href="componentes/css/estilo.css" rel="stylesheet" type="text/css" />
    <link href="componentes/css/foundation.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="componentes/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="componentes/css/jquery-ui-1.10.4.custom.css" />
    <!-- jQuery-->
    <script src="componentes/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="componentes/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="componentes/js/foundation.min.js" type="text/javascript"></script>
</head>
<body>