
<?php
include('header.php');
?>
<script type="text/javascript">
    $(document).ready(function(){
        //BUSCAR Y MOSTRAR RESULTADOS DE REGISTROS
        append_datos();
        ///VALIDACIONES DE CAMPOS
        solo_num("#telf,#cedula,#filter");
        solo_letra("#name");
        //FORMULARIO DE REGISTRO DE DATOS
        $("#save").on("click", function(){
            var cedula = $("#cedula").val();
            var nombre = $("#name").val();
            var sexo = $(".genero").val();
            var direccion = $("#direccion").val();
            var telf = $("#telf").val();
            $("#confirmAdd").dialog({
                modal: true,
                title: "Comfirmar",
                width: 300,
                show: "fold",
                hide: "clip",
                buttons: {
                    "Confirmar": function () {
                        if(cedula.length>0 && nombre.length>0 && sexo.length>0 && direccion.length>0 && telf.length>0){
                            $.ajax({
                                url: "controlador/acciones.php",
                                data: {
                                    oper: 'add',
                                    cedula: cedula,
                                    nombre:nombre,
                                    sexo:sexo,
                                    direccion:direccion,
                                    telf:telf
                                },
                                type: "POST",
                                success: function (ret) {
                                    if(ret==1){
                                        limpiar();
                                        append_datos();
                                        //alert("Registro Exitoso");
                                    }
                                }
                            });
                        }else{
                            alert("Por favor complete los datos para continuar");
                        }
                        $(this).dialog("close");
                    },
                    "Cancelar": function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
        //Function limpiar campos
        $("#clear").on("click",function(){
            limpiar ();
        });
        function limpiar (){
            $('.genero').attr('checked', false);
            $("#cedula,#name,#direccion,#telf").val('');
        }
        //Function ver lista de registros
        function append_datos(){
            var dato =  $("#filter").val();
            var filtro = dato ? dato : 0;
            $.ajax({
                url: "controlador/acciones.php",
                data: {
                    oper: 'consulta',
                    filtro:filtro
                },
                type: "POST",
                dataType: "json",
                success: function (ret) {
                    //var objeto = ret.rows[0].cell;
                    var cant = ret.rows.length;
                    if(cant>0){
                        $(".block_resultados").removeClass('hide');
                    }
                    $("#rows_result div").remove('');
                    for (var i = 0; i<cant; i++) {
                        var cedula = ret.rows[i].cell[0];
                        var nombre = ret.rows[i].cell[1];
                        var sexo = ret.rows[i].cell[2];
                        var direccion = ret.rows[i].cell[3];
                        var telf = ret.rows[i].cell[4];
                        var id = ret.rows[i].cell[5];
                        var p = document.createElement("p");
                        var b = document.createElement("b");
                        var bnode = document.createTextNode((i + 1) + ' ');
                        var textnode = document.createTextNode(cedula + ' ' + nombre + ' ' + sexo + ' ' + direccion + ' ' + telf);
                        b.appendChild(bnode);
                        p.appendChild(b);
                        p.appendChild(textnode);
                        $("#rows_result").append("<div class='small-12 large-10 medium-10 columns div_rows div"+id+"' id=row_here"+id+"></div>" +
                            "<div class='small-12 large-2 medium-2 columns div"+id+"'><span id=edit"+id+" class='bottom_accion edit' data-reveal-id='modalEdit'><small class='fa fa-edit'></small></span>" +
                            "&nbsp;&nbsp;<span id=dell"+id+" class='bottom_accion dell'><small class='fa fa-times'></small></span></div>");
                         document.getElementById('row_here'+id).appendChild(p);

                        //Function Editar
                        $(".edit").on ("click", function(){
                            var valor = $(this).attr("id");
                            var id = valor.replace('edit','');
                            //Buscar Datos
                            $.ajax({
                                url: "controlador/acciones.php",
                                data: {
                                    oper: 'consulta_rows',
                                    id: id
                                },
                                dataType: "json",
                                type: "POST",
                                success: function (ret) {
                                    var cedula = ret.rows[0].cell[0];
                                    var nombre = ret.rows[0].cell[1];
                                    var sexo = ret.rows[0].cell[2];
                                    var direccion = ret.rows[0].cell[3];
                                    var telf = ret.rows[0].cell[4];
                                    $("#modalEdit input").remove('');
                                    $('#input_cell').append("<div class='small-12 medium-4 large-4 columns'><input type= 'text' id= 'ced' value = "+cedula+" maxlength='10'></div>" +
                                        "<div class='small-12 medium-6 large-6  columns'><input type= 'text' id= 'namex' maxlength='70'></div>" +
                                        "<div class='small-12 medium-2 large-2 columns'><input type='text' id='generox' value = "+sexo+" maxlength='1'></div>" +
                                        "<div class='small-12 medium-6 large-6 columns'><input type= 'text' id= 'direc' maxlength='70'></div>" +
                                        "<div class='small-12 medium-6 large-6 columns'><input type= 'text' id= 'telfx' value = "+telf+" maxlength='11'></div> ");
                                    $(".editData").attr("id",id);
                                    $("#namex").val(nombre); $("#direc").val(direccion);
                                    $("#generox").keyup(function(){
                                        $(this).css("text-transform", "uppercase");
                                    });
                                    solo_num("#ced,#telfx");
                                    solo_letra("#namex,#generox");
                                }
                            });
                            //Editar
                            $(".editData").on("click", function(){
                                var id_cell = $(this).attr("id");
                                var cedula = $("#ced").val();
                                var nombre = $("#namex").val();
                                var sexo = $("#generox").val();
                                var direccion = $("#direc").val();
                                var telf = $("#telfx").val();
                                $("#confirmEdit").dialog({
                                    modal: true,
                                    title: "Comfirmar",
                                    width: 300,
                                    show: "fold",
                                    hide: "clip",
                                    buttons: {
                                        "Confirmar": function () {
                                            if(cedula.length>0 && nombre.length>0 && (sexo.length=1 && (sexo == 'M' || sexo == 'F')) && direccion.length>0 && telf.length>0){
                                                $.ajax({
                                                    url: "controlador/acciones.php",
                                                    data: {
                                                        oper: 'edit',
                                                        id: id_cell,
                                                        cedula: cedula,
                                                        nombre:nombre,
                                                        sexo:sexo,
                                                        direccion:direccion,
                                                        telf:telf
                                                    },
                                                    type: "POST",
                                                    success: function (ret) {
                                                        if(ret==1){
                                                            append_datos();
                                                            $('#modalEdit').foundation('reveal', 'close');
                                                            $("#msj_editresult").show();
                                                            jQuery("#msj_editresult").fadeOut(6000);
                                                            $("#error_edit").hide();
                                                        }
                                                    }
                                                });
                                            }else{
                                                if(sexo != 'M' || sexo != 'F'){
                                                    alert("Por favor verifique el sexo");
                                                }
                                                $("#error_edit").show();
                                            }
                                            $(this).dialog("close");
                                        },
                                        "Cancelar": function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                });

                            });

                        });
                        //Function Eliminar
                        $(".dell").on ("click", function(){
                            var valor = $(this).attr("id");
                            var id = valor.replace('dell','');
                            $("#confirmDell").dialog({
                                modal: true,
                                title: "Comfirmar",
                                width: 300,
                                show: "fold",
                                hide: "clip",
                                buttons: {
                                    "Confirmar": function () {
                                        $(".div"+id).hide();
                                        $.ajax({
                                            url: "controlador/acciones.php",
                                            data: {
                                                oper: 'dell',
                                                id: id
                                            },
                                            type: "POST",
                                            success: function (ret) {
                                                $("#msj_delresult").show();
                                                jQuery("#msj_delresult").fadeOut(6000);
                                                append_datos();
                                                $("#row_here"+id).hide();
                                            }
                                        });
                                        $(this).dialog("close");
                                    },
                                    "Cancelar": function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        });
                    }
                    //fin loop
                }
            });
        }
        //VERIFICA CANTIDAD DE REGISTROS
        $("#search").on("click",function(){
            var filtro =  $("#filter").val();
            if(filtro.length>0){
                $.ajax({
                    url: "controlador/acciones.php",
                    data: {
                        oper: 'cantidad',
                        filtro: filtro
                    },
                    type: "POST",
                    success: function (ret) {
                       if(ret>0){
                           append_datos ();
                           $("#rows_result").show();
                           $("#msj_noresult").hide();
                       }else{
                           $("#msj_noresult").show();
                           $("#rows_result").hide();
                       }
                    }
                });
            }else{
                append_datos ();
                $("#rows_result").show();
                $("#msj_noresult").hide();
            }
        });
        //LIMPIAR FILTRO DE BUSQUEDA
        $("#delete").on("click",function(){
            $("#filter").val('');
            append_datos ();
            $("#rows_result").show();
            $("#msj_noresult").hide();
        });
        $("#imprimir").on("click", function(){
            var config = "menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no";
            window.open("rpt_listado.php",config, 800,600);
        });
    });
    function solo_num(val){
        $(val).keyup(function(){
            var $th = $(this);
            $th.val( $th.val().replace(/[a-zA-Z]/gi, function(){ return '';}) );
        });
    }
    function solo_letra(val){
        $(val).keyup(function(){
            var $th = $(this);
            $th.val( $th.val().replace(/[0-9-.,':_?=]/gi, function(){ return ''; }) );
        });
    }

</script>
<div class="row">
    <div class="small-12 columns text-center" id="titul_gral">
        <p>< MI PHP ></p><span id="descrip_gral">Acciones basicas: Agregar, modificar, eliminar y mostrar resultados</span>
    </div>
</div>
<!---- COMENCEMOS AGREGAR CONTENIDO ---->
<div class="row" id="add_form">
    <div class="small-12 columns" id="blok_datos">
        <p><span class="fa fa-pencil"></span> REGISTRO DE DATOS</p>
    </div>
    <div class="small-12 columns">
        <div class="small-12 large-4 medium-4 columns">
            <label><b>*</b> Cédula de identidad</label>
            <input type="text" id="cedula" placeholder="Indique la cedula del estudiante" maxlength="8">
        </div>
        <div class="small-12 large-5 medium-5 columns">
            <label><b>*</b> Nombre y apellido</label>
            <input type="text" id="name" placeholder="Indique el nombre completo" maxlength="70">
        </div>
        <div class="small-12 large-3 medium-3 columns div_genero">
            <label><b>*</b> Sexo</label>
            <span><span class="fa fa-female"></span> F</span> <input type="radio" name="sexo" id="F" class="genero" value="F">&nbsp;
            <span><span class="fa fa-male"></span> M</span> <input type="radio" name="sexo" id="M" class="genero" value="M">
        </div>
    </div>
    <div class="small-12 columns">
        <div class="small-12 large-8 medium-8 columns">
            <label><b>*</b> Dirección de domicilio</label>
            <input type="text" id="direccion" placeholder="Indique la dirección completa" maxlength="70">
        </div>
        <div class="small-12 large-4 medium-4 columns">
            <label><b>*</b> Teléfono Movil</label>
            <input type="text" id="telf" placeholder="Indique el número de teléfono" maxlength="11">
        </div>
    </div>
    <div class="small-12 columns">
        <div class="small-12 columns">
            <button type="submit" id="save"><small class="fa fa-save"></small> Registrar</button>
            <button type="reset" id="clear"><small class="fa fa-eraser"></small> Limpiar</button>
        </div>
    </div>
</div>
<!-----RESULTADO DE REGISTROS----->
<div class="row block_resultados hide">
    <div class="small-12 columns" id="blok_datos">
        <p><span class="fa fa-list"></span> RESULTADOS DE LA CONSULTA</p>
    </div>
</div>
<!----ELEMENTO DE BUSQUEDA----->
<div class="row block_resultados hide">
    <div class="small-6 medium-4 large-3 columns block_search"><input type="text" placeholder="Indique número de cédula" id="filter" maxlength="8"></div>
    <div class="small-6 medium-4 large-3 columns block_search left"><span class="fa fa-search icon_search" id="search"></span><span class="fa fa-eraser icon_clear" id="delete"></span></div>
</div>

<!----MENSAJE DE ELIMINACIÓN EXITOSA----->
<div class="row" id="msj_delresult" style="display: none">
    <div class="small-12 columns">
        <div data-alert class="alert-box alert radius" >
            REGISTRO ELIMINADO EXITOSAMENTE
            <a href="#" class="close">&times;</a>
        </div>
    </div>
</div>

<!----MENSAJE DE MODIFICACIÓN EXITOSA----->
<div class="row" id="msj_editresult" style="display: none">
    <div class="small-12 columns">
        <div data-alert class="alert-box alert radius" >
            REGISTRO MODIFICADO EXITOSAMENTE
            <a href="#" class="close">&times;</a>
        </div>
    </div>
</div>

<!----LISTA DE RESULTADOS----->
<div class="row" id="rows_result"></div>
<!----MENSAJE DE BUSQUEDA SIN RESULTADOS----->
<div class="row" id="msj_noresult" style="display: none">
    <div class="small-12 columns">
        <div data-alert class="alert-box alert radius" >
            NO SE ENCONTRARON RESULTADOS
            <a href="#" class="close">&times;</a>
        </div>
    </div>
</div>
<!----ACTUALIZAR DATOS------>
<div class="row">
    <div class="large-12 small-12 columns">
        <div id="modalEdit" class="reveal-modal" data-reveal aria-labelledby="Editar datos" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <div class="small-12 columns" id="blok_datos">
                <p><span class="fa fa-pencil"></span> ACTUALIZAR DATOS DEL ESTUDIANTE</p>
                <div id="input_cell"></div>
            </div>
            <div class="small-12 columns">
                <div class="small-12 columns">
                    <button type="submit" class="editData">Modificar</button>
                </div>
            </div>
            <div class="small-12 columns hide" id="error_edit">
                <div class="small-12 columns">
                    <p><span class="fa fa-search"></span> Por favor complete los datos</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="small-12 columns">
    <div class="small-12 large-8 medium-8 columns text-right">
        <button type="submit" id="imprimir"><small class="fa fa-save"></small> Imprimir</button>
    </div>
</div>
<?php
include_once('footer.php');
?>
<!----CONFIRMAR CAMBIOS------>
<div id="confirmAdd" style="display:none"><span>Seguro que desea registrar los datos?</span></div>
<div id="confirmEdit" style="display:none"><span>Seguro que desea actualizar los datos?</span></div>
<div id="confirmDell" style="display:none"><span>Seguro que desea eliminar el registro?</span></div>