<!-----PIE DE PAGINA----->
<div class="row footer_php">
    <div class="hide-for-small-only margin_bottom_contact2"></div>
    <div class="small-12 columns text-center">
        <p class="text_footer"><span class="fa fa-copyright"></span> Derechos Reservados Karina Torres, <?php echo gmdate("Y");?></p>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(document).foundation();
    });
</script>

</body>
</html>