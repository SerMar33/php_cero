<?php
session_start();
include('connection.php');
$connection = conecta ();
require('controlador/ajustar_reporte.php');
$id=$_GET['id'];
//**OBTENER VARIABLES**//

$pdf=new PDF_MC_Table($orientation='L',$unit='mm',$format='letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);
$pdf->Ln(10);
$pdf->MultiCell(0,5,html_entity_decode('LISTADO DE ESTUDIANTES'),'','C');
$pdf->Ln(20);
$pdf->SetFont('Arial','B',10);
$columns = array();
$col = array();
$col[]   = array('text' => utf8_decode('Cédula'),'width' => '30', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '248,248,248', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'TBLR');
$col[]   = array('text' => 'Nombre y apellido','width' => '70', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '248,248,248', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'TBLR');
$col[]   = array('text' => 'Sexo','width' => '15', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '248,248,248', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'TBLR');
$col[]   = array('text' => utf8_decode('Dirección'),'width' => '90', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '248,248,248', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'TBLR');
$col[]   = array('text' => utf8_decode('Teléfono'),'width' => '30', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '248,248,248', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'TBLR');

$columns[] = $col;
$pdf->SetX(12);
$pdf->WriteTable($columns);
/****Consulta*******/
//$result=mysql_query("select * from estudiante");//Aqui haces la consulta en la Base de datos y muestras los Datos en la web

if(isset($id)){$where = "Where estudiante_ced= ".$id."";}else {$where = "";}
$consulta = "SELECT estudiante_ced,estudiante_nombre,estudiante_sexo,estudiante_direcc,estudiante_telf,estudiante_id from estudiante ".$where." ORDER BY estudiante_id DESC";

$result = mysqli_query($connection,$consulta);

while ($row= mysqli_fetch_assoc($result)){
    $columns = array();
    $col     = array();
    $col[]   = array('text' => $row['estudiante_ced'],'width' => '30', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'LTBR');
    $col[]   = array('text' => ucwords(strtolower(utf8_decode($row['estudiante_nombre']))),'width' => '70', 'height' => '6', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'LTBR');
    $col[]   = array('text' =>ucwords(strtolower(utf8_decode($row['estudiante_sexo']))),'width' => '15', 'height' => '6', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'LTBR');
    $col[]   = array('text' =>ucwords(strtolower(utf8_decode($row['estudiante_direcc']))),'width' => '90', 'height' => '6', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'LTBR');
    $col[]   = array('text' =>ucwords(strtolower(utf8_decode($row['estudiante_telf']))),'width' => '30', 'height' => '6', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '32,32,32', 'drawcolor' => '0,0,0', 'linewidth' => '0.05', 'linearea' => 'LTBR');
    $columns[] = $col;
    $pdf->SetX(12);
    $pdf->WriteTable($columns);
}
/***Fin**/
$pdf->Ln(2);
$pdf->SetX(28);

$pdf->Output('reporte.pdf','I');
$pdf->Output();
?>