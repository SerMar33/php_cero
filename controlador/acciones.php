<?php
include('../modelos/acciones.class.php');
$connection = conecta ();
$conecta = new acciones;
$oper = $_POST['oper'];
switch ($oper){
    case 'add':
        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $sexo = $_POST['sexo'];
        $direccion = $_POST['direccion'];
        $telf = $_POST['telf'];
        if($cedula && $nombre && $direccion && $telf)
        {
            $ejecuta = $conecta->add_estudiante($cedula, $nombre, $sexo, $direccion, $telf);
            echo $ejecuta;
        }
        break;
    case 'cantidad':
        $filtro = $_POST['filtro'];
        $where = "Where estudiante_ced= ".$filtro." ";
        $consulta = "SELECT estudiante_ced,estudiante_nombre,estudiante_sexo,estudiante_direcc,estudiante_telf,estudiante_id from estudiante ".$where." ORDER BY estudiante_id DESC";
        $resultado = mysqli_query($connection,$consulta);
        $row_cnt = $resultado->num_rows;
        echo $row_cnt;
        break;
    case 'consulta':
        if($_POST['filtro']!=0){
            $filtro = $_POST['filtro'];
            $where = "Where estudiante_ced= ".$filtro." ";
        }else{
            $where = '';
        }
        $consulta = "SELECT estudiante_ced,estudiante_nombre,estudiante_sexo,estudiante_direcc,estudiante_telf,estudiante_id from estudiante ".$where." ORDER BY estudiante_id DESC";
        $resultado = mysqli_query($connection,$consulta);
        $datos = array();
        $responce = array();
        $i=0;
        while ($registro= mysqli_fetch_assoc($resultado)){
            $datos[]=$registro;
        }
        foreach ($datos as $row) {
            $responce['rows'][$i]['cell']=array(
                $row['estudiante_ced'],
                $row['estudiante_nombre'],
                $row['estudiante_sexo'],
                $row['estudiante_direcc'],
                $row['estudiante_telf'],
                $row['estudiante_id']
            );
            $i++;
        }
        $cant = count($datos);
        echo json_encode($responce);

        break;
    case 'consulta_rows':
        $id = $_POST['id'];
        $consulta = "SELECT estudiante_ced,estudiante_nombre,estudiante_sexo,estudiante_direcc,estudiante_telf,estudiante_id from estudiante WHERE estudiante_id='".$id."'  ORDER BY estudiante_id DESC";
        $resultado = mysqli_query($connection,$consulta);
        $datos = array();
        $responce = array();
        $i=0;
        while ($registro= mysqli_fetch_assoc($resultado)){
            $datos[]=$registro;
        }
        foreach ($datos as $row) {
            $responce['rows'][$i]['cell']=array(
                $row['estudiante_ced'],
                $row['estudiante_nombre'],
                $row['estudiante_sexo'],
                $row['estudiante_direcc'],
                $row['estudiante_telf'],
                $row['estudiante_id']
            );
            $i++;
        }
        echo json_encode($responce);
        break;
    case 'edit':
        $id = $_POST['id'];
        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $sexo = $_POST['sexo'];
        $direccion = $_POST['direccion'];
        $telf = $_POST['telf'];
        $ejecuta = $conecta->edit_estudiante($id,$cedula, $nombre, $sexo, $direccion, $telf);
         echo $ejecuta;
        break;
    case 'dell':
        $id = $_POST['id'];
        $sql = "DELETE FROM estudiante WHERE estudiante_id=".$id;
        $result = mysqli_query($connection,$sql);
        echo $result;
        break;
    default:
        echo "No se consigueron resultados";

}
?>